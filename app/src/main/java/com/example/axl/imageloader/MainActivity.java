package com.example.axl.imageloader;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    //Ces variables sont moches, mais je les aime quand même.
    ArrayList<String> urls = new ArrayList<>();
    int counter = 1;
    ImageLoader img;
    ImageView imgv;
    TextView txtv;
    Calendar cal = Calendar.getInstance();
    float x1, x2;
    float y1, y2;
    String image = "";
    String imei = "354792058973433";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        TelephonyManager tMgr = (TelephonyManager) MainActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
//        String mPhoneNumber = tMgr.getDeviceId();
//
//        if (mPhoneNumber != imei) {
//            this.finishAffinity();
//        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ImageView imageView = (ImageView) findViewById(R.id.imageView);
        final ImageLoader imageLoader = ImageLoader.getInstance();

        Toast.makeText(getApplicationContext(), "Swippez à gauche pour voir les précédentes ! :D", Toast.LENGTH_LONG).show();

        TextView date = (TextView) findViewById(R.id.textView);
        //date.setText(constructDate());

        try {
            urls.add(getImages("http://ditesbonjouralamadame.tumblr.com/"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        img = imageLoader;
        imgv = imageView;
        txtv = date;

        imageLoader.displayImage(urls.get(counter - 1), imageView);
    }

    public String constructDate() {
        String month = "";
        String date;
        switch (cal.get(Calendar.MONTH)) {
            case 0: month = "Janvier";
                break;
            case 1: month = "Fevrier";
                break;
            case 2: month = "Mars";
                break;
            case 3: month = "Avril";
                break;
            case 4: month = "Mai";
                break;
            case 5: month = "Juin";
                break;
            case 6: month = "Juillet";
                break;
            case 7: month = "Aout";
                break;
            case 8: month = "Septembre";
                break;
            case 9: month = "Octobre";
                break;
            case 10: month = "Novembre";
                break;
            case 11: month = "Décembre";
                break;
        }
        date = cal.get(Calendar.DATE) + month;
        return date;
    }

    public String getImages(final String url) throws IOException {
        image = ""; //on reset l'image pour eviter des problemes de synchro
        new Thread() {
            public void run() {
                try {
                    Document doc = Jsoup.connect(url).get();//fetch page at url
                    Elements links = doc.select("img");
                    String test = links.get(3).attr("abs:src");
                    image = test;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
        while (image == "") {} //on fait en sorte que l'assignation soit faite avant de retourner la valeur
        return image;
    }

    public boolean onTouchEvent(MotionEvent touchevent) { //touchevent = touch event MAIS touche vent c'est beaucoup plus classe

        switch (touchevent.getAction()) {
            // when user first touches the screen we get x and y coordinate
            case MotionEvent.ACTION_DOWN: {
                x1 = touchevent.getX();
                y1 = touchevent.getY();
                break;
            }
            case MotionEvent.ACTION_UP: {
                x2 = touchevent.getX();
                y2 = touchevent.getY();

                //if left to right sweep event on screen
                if (x1 < x2) {

                    try {
                        if (counter == urls.size()) {
                            String newImage = getImages("http://ditesbonjouralamadame.tumblr.com/page/" + String.valueOf(counter + 1));
                            urls.add(newImage);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtv.setText(""); //TODO change date to fit image date
                    ++counter;
                    img.displayImage(urls.get(counter - 1), imgv);
                }

                // if right to left sweep event on screen
                if (x1 > x2) {
                    if (counter - 1 > 0) {
                        --counter;
                        img.displayImage(urls.get(counter - 1), imgv);
                    }
                }

                // if UP to Down sweep event on screen
                if (y1 < y2) {
                }

                //if Down to UP sweep event on screen
                if (y1 > y2) {
                }
                break;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
